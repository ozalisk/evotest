from flask import Flask, render_template, request, flash, redirect, url_for
from forms import noteForm
from pymongo import MongoClient, DESCENDING
from os import getenv

# Зі змінних оточення вибираємо потрібні дані, які не зберігаються в системі контролю версій

MONGO_USERNAME=getenv('MONGO_USERNAME')
MONGO_PASSWORD=getenv('MONGO_PASSWORD')
SECRET_KEY=getenv('SECRET_KEY')
WTF_CSRF_SECRET_KEY=getenv('WTF_CSRF_SECRET_KEY')

flask_app = Flask(__name__)
flask_app.config.update({'WTF_CSRF_SECRET_KEY':WTF_CSRF_SECRET_KEY,
                   'SECRET_KEY': SECRET_KEY})

# Налаштовуємо зєднання з базою даних
client = MongoClient("mongodb+srv://{0}:{1}@cluster0-ecasv.mongodb.net/test".format(MONGO_USERNAME, MONGO_PASSWORD))
db = client.get_database('evotest')

@flask_app.route('/')
def hello_world():
    flash('Вітаю! До Вашої уваги виконана перша частина тестового завдання. ')
    return redirect(url_for('notes'))

@flask_app.route('/notes')
def notes():
    available_notes = list(db.notes.find({},{'text':1,'unique_words_count':1, '_id':0}).sort('unique_words_count',DESCENDING))
    available_notes_texts = ["{0} => {1} слів".format(note['text'],note['unique_words_count']) for note in available_notes]
    if len(available_notes) == 0:
        flash('Ніяких приміток в базу даних не додано. Будьте першим, хто зберіг свою примітку!')
    return render_template("maintemplate.html", notes=available_notes_texts)

@flask_app.route('/addnote')
def addnote():
    form = noteForm()
    return render_template("maintemplate.html", form=form)


@flask_app.route('/api/submit', methods=["GET", "POST"])
def submitnote():
    # приймаємо форму,
    form = noteForm(request.form)
    # беремо з примітку
    note = form.content.data

    # Обробка отриманої у формі примітки
    record_note_to_db(note)
    flash("Примітку додано")
    return redirect(url_for("notes"))


def record_note_to_db(note):
    # порахуємо унікальні слова в уривкові тексту
    n = n_unique(note)
    # Зберігаємо в базу даних примітку і кількість слів
    db.notes.insert_one({'text':note, 'unique_words_count':n})

    # Метод повертає ціле число - кількість унікальних слів у рядку
def n_unique(note):

    # Формуємо список неалфавітних символів
    delimiters = [symbol for symbol in note.lower() if not symbol.isalpha()]

    # Зі списку неалфавітних символів видаляємо ті, що повторюються
    delimiters = set(delimiters)

    # Ця змінна міститиме рядок-аргумент, у якому неалфавітні символи замінені на пробіли.
    # Пробіли - для того, щоб потім було зручно розділити рядок на слова
    note1=''

    # Заміна за один прохід по тексту всих наявних неалфавітних символів і внесення результату в нову змінну
    for symbol in note:
        tobeadded = symbol if symbol not in delimiters else ' '
        note1+=tobeadded

    # Розділюємо очищений від неалфавітних символів рядок на слова
    words = note1.split(' ')

    # Якщо десь попався пустий стрінг, видаляємо його
    words = [word for word in words if len(word)>0]

    # Видаляємо неунікальні слова
    wordlist = set(words)

    # Довжина сформованого списку неунікальних слів
    return len(wordlist)

if __name__ == '__main__':
    flask_app.run()